import {Component, OnInit} from '@angular/core';
import {Meta, Title} from '@angular/platform-browser';

@Component({
  selector: 'app-pre-page',
  templateUrl: './pre-page.component.html',
  styleUrls: ['./pre-page.component.scss']
})
export class PrePageComponent implements OnInit {

  public imgLoaded = false;

  constructor(private titleService: Title, private meta: Meta) {
    this.meta.addTags([{name: 'description', content: 'BMB Assurances, spécialiste de l’assurance de prêt\n' +
        '  Fort de nos 10 ans d\'expérience, notre connaissance approfondie du marché et des acteurs du\n' +
        '  secteur (compagnies d\'assurance et banques) nous permet de vous apporter notre expertise.'}]);
  }

  ngOnInit() {
    this.titleService.setTitle('BMB Assurances : courtier en assurance de prêt  | BMB Assurances');
  }


  onImageLoad(event) {
    this.imgLoaded = true;
  }
}
