import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrePageComponent } from './pre-page.component';

describe('PrePageComponent', () => {
  let component: PrePageComponent;
  let fixture: ComponentFixture<PrePageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrePageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
