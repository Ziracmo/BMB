import {Component, ElementRef, HostListener} from '@angular/core';

@Component({
  selector: 'app-companies-bandeau',
  templateUrl: './companies-bandeau.component.html',
  styleUrls: ['./companies-bandeau.component.scss']
})
export class CompaniesBandeauComponent {

  translate = 0;
  imageWidth = 180;
  path = '../assets/img/logos/';
  logos = [
    'AFI-ESCA',
    'APRIL',
    'CARDIF',
    'METLIFE',
    'MMA',
    'NAO',
    'SPHERIA',
    'SWISSLIFE',
    'UGIP',
    'UTWIN'
  ];

  constructor(private elem: ElementRef) {
  }

  @HostListener('window:resize')
  onResize() {
    const logos = this.elem.nativeElement.querySelectorAll('li');
    this.translate = 0;
    logos.forEach((logo) => {
      logo.style.transform = `translateX(${this.translate}px)`;
    });
  }

  moveRight() {
    const logos = this.elem.nativeElement.querySelectorAll('li');
    const ulWidth = this.elem.nativeElement.querySelector('ul').clientWidth;
    const allImageWidth = this.imageWidth * this.logos.length;
    const gap = allImageWidth - ulWidth;
    const space = gap / 2 + this.translate;

    if (space > this.imageWidth) {
      this.translate -= this.imageWidth;
    } else if (space > 0) {
      this.translate -= space;
    }

    logos.forEach((logo) => logo.style.transform = `translateX(${this.translate}px)`);
  }

  moveLeft() {
    const logos = this.elem.nativeElement.querySelectorAll('li');
    const ulWidth = this.elem.nativeElement.querySelector('ul').clientWidth;
    const allImageWidth = this.imageWidth * this.logos.length;
    const gap = allImageWidth - ulWidth;
    const space = gap / 2 - this.translate;

    if ((allImageWidth - ulWidth) / 2 + this.translate > this.imageWidth) {
      this.translate += this.imageWidth;
    } else if (space > 0) {
      this.translate += space;
    }

    logos.forEach((logo) => {
      logo.style.transform = `translateX(${this.translate}px)`;
    });
  }
}
