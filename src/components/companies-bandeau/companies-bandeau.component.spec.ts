import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompaniesBandeauComponent } from './companies-bandeau.component';

describe('CompaniesBandeauComponent', () => {
  let component: CompaniesBandeauComponent;
  let fixture: ComponentFixture<CompaniesBandeauComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompaniesBandeauComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompaniesBandeauComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
