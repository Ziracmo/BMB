import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-change',
  templateUrl: './change.component.html',
  styleUrls: ['./change.component.scss']
})
export class ChangeComponent implements OnInit, AfterViewInit {

  public imgLoaded = false;

  constructor(private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle('BMB Assurances : changez d\'assurance de prêt | BMB Assurances');
  }

  ngAfterViewInit() {
    document.getElementsByTagName('body')[0].scrollIntoView();
  }

  onImageLoad(event) {
    this.imgLoaded = true;
  }

}
