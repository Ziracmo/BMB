import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-compare',
  templateUrl: './compare.component.html',
  styleUrls: ['./compare.component.scss']
})
export class CompareComponent implements OnInit, AfterViewInit {

  isLoading = true;

  constructor(private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle('BMB Assurances : comparez et économisez | BMB Assurances');
  }

  onLoad() {
    this.isLoading = false;
  }

  ngAfterViewInit() {
    document.getElementsByTagName('body')[0].scrollIntoView();
  }

}
