import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-become-ambassador',
  templateUrl: './become-ambassador.component.html',
  styleUrls: ['./become-ambassador.component.scss']
})
export class BecomeAmbassadorComponent implements OnInit, AfterViewInit {

  public imgLoaded = false;

  constructor(private titleService: Title) { }

  ngOnInit() {
    this.titleService.setTitle('BMB Assurance - Devenez ambassadeur');
  }

  ngAfterViewInit() {
    document.getElementsByTagName('body')[0].scrollIntoView();
  }

  onImageLoad(event) {
    this.imgLoaded = true;
  }


}
