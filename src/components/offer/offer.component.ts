import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {ActivatedRoute} from '@angular/router';
import {Observable} from 'rxjs';


@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.scss']
})
export class OfferComponent implements OnInit, AfterViewInit {

  observable: Observable<any>;

  constructor(private titleService: Title, private route: ActivatedRoute) {
    this.observable = this.route.params;
  }

  ngOnInit() {
    this.titleService.setTitle('BMB Assurances : nos offres d\'assurance de prêt');
  }

  ngAfterViewInit() {
  this.observable.subscribe(params => {
      if (params.section) {
        const el = document.getElementById(params.section);
        switch (params.section) {
          case 'risques':
            el.scrollIntoView();
            break;
          case 'capitaux':
            el.scrollIntoView();
            break;
          case 'professions':
            el.scrollIntoView();
            break;
          case 'expatriés':
            el.scrollIntoView();
            break;
        }
      } else {
        document.getElementsByTagName('body')[0].scrollIntoView();
      }
    });
  }

}
