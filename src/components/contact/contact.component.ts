import {AfterViewInit, Component, OnInit} from '@angular/core';
import {environment} from '../../environments/environment';
import {Title} from '@angular/platform-browser';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import * as Noty from 'noty';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit, AfterViewInit {

  model: any = {};

  constructor(private titleService: Title, private http: HttpClient) {
  }

  ngOnInit() {
    this.titleService.setTitle('BMB Assurances : contactez-nous | BMB Assurances');
  }

  ngAfterViewInit() {
    document.getElementsByTagName('body')[0].scrollIntoView();
  }

  onSubmit() {
    this.http.post(environment.mailUrl, this.model, {responseType: 'text'}).subscribe(res => {
      new Noty({
        text: 'Le message a bien été envoyé',
        type: 'success',
        theme: 'relax',
        timeout: 3000
      }).show();

      this.model = {
        sujet: '',
        message: '',
      };
    });
  }

}
