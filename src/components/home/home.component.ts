import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit{

  public imgLoaded = false;

  constructor(private titleService: Title) {
  }

  ngOnInit() {
    this.titleService.setTitle('BMB Assurance - Courtier spécialiste de l\'assurance de prêt\n');
  }

  ngAfterViewInit() {
    document.getElementsByTagName('body')[0].scrollIntoView();
  }

  onImageLoad(event) {
    this.imgLoaded = true;
  }
}
