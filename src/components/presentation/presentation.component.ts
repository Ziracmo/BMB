import { Component, OnInit } from '@angular/core';
import {Title} from '@angular/platform-browser';

@Component({
  selector: 'app-presentation',
  templateUrl: './presentation.component.html',
  styleUrls: ['./presentation.component.scss']
})
export class PresentationComponent implements OnInit {

  public imgLoaded = false;

  constructor(private titleService: Title) {
  }

  ngOnInit() {
    this.titleService.setTitle('BMB Assurances : qui sommes-nous ?');
  }

  onImageLoad(event) {
    this.imgLoaded = true;
  }

}
