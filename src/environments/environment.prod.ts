export const environment = {
  production: true,
  intercomAppId: 'ws7qh2jc',
  mailUrl: 'https://www.bmb-assurances.fr/mail.php'
};
