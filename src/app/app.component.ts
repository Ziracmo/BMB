import {Component, OnInit} from '@angular/core';
import {environment} from '../environments/environment';
import {Intercom} from 'ng-intercom';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  constructor(public intercom: Intercom) {

  }

  ngOnInit() {
    this.intercom.boot({
      app_id: environment.intercomAppId,
      // Supports all optional configuration.
      widget: {
        'activator': '#intercom'
      }
    });
  }
}
