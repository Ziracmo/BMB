import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {PrePageComponent} from '../components/pre-page/pre-page.component';
import {HomeComponent} from '../components/home/home.component';
import {BecomeAmbassadorComponent} from '../components/become-ambassador/become-ambassador.component';
import {ContactComponent} from '../components/contact/contact.component';
import {PresentationComponent} from '../components/presentation/presentation.component';
import {OfferComponent} from '../components/offer/offer.component';
import {ChangeComponent} from '../components/change/change.component';
import {CompareComponent} from '../components/compare/compare.component';
import {PageNotFoundComponent} from '../components/page-not-found/page-not-found.component';


const appRoutes: Routes = [
  {path: '', component: PrePageComponent},
  {path: 'accueil', component: HomeComponent},
  {path: 'devenir-ambassadeur', component: BecomeAmbassadorComponent},
  {path: 'contact', component: ContactComponent},
  {path: 'presentation', component: PresentationComponent},
  {
    path: 'nos-offres', component: OfferComponent,
  },
  {
    path: 'nos-offres/:section', component: OfferComponent,
  },
  {path: 'changer', component: ChangeComponent},
  {path: 'comparer', component: CompareComponent},
  {
    path: '',
    redirectTo: '/',
    pathMatch: 'full'
  },
  {path: '**', component: PageNotFoundComponent}
];


@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
