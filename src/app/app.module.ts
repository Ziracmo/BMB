import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
// @ts-ignore
import {IntercomModule} from 'ng-intercom';
import {HomeComponent} from '../components/home/home.component';
import {ContactComponent} from '../components/contact/contact.component';
import {PageNotFoundComponent} from '../components/page-not-found/page-not-found.component';
import {OfferComponent} from '../components/offer/offer.component';
import {HeaderComponent} from '../components/header/header.component';
import {ChangeComponent} from '../components/change/change.component';
import {CompareComponent} from '../components/compare/compare.component';
import {LoaderComponent} from '../components/loader/loader.component';
import {CompaniesBandeauComponent} from '../components/companies-bandeau/companies-bandeau.component';
import {PresentationComponent} from '../components/presentation/presentation.component';
import {BecomeAmbassadorComponent} from '../components/become-ambassador/become-ambassador.component';
import {MentionsComponent} from '../components/mentions-modal/mentions.component';
import {ConditionsModalComponent} from '../components/conditions-modal/conditions-modal.component';
import {PrePageComponent} from '../components/pre-page/pre-page.component';
import {HttpClientModule} from '@angular/common/http';
import {FooterModule} from '../components/footer/footer.module';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ContactComponent,
    HeaderComponent,
    PageNotFoundComponent,
    OfferComponent,
    ChangeComponent,
    CompareComponent,
    LoaderComponent,
    CompaniesBandeauComponent,
    PresentationComponent,
    PrePageComponent,
    ConditionsModalComponent,
    MentionsComponent,
    BecomeAmbassadorComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    IntercomModule.forRoot({
      appId: 'ws7qh2jc', // from your Intercom config
      updateOnRouterChange: true // will automatically run `update` on router event changes. Default: `false`
    }),
    HttpClientModule,
    FooterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
